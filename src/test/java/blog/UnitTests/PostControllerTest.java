package blog.UnitTests;

import blog.app.controllers.PostsController;
import blog.lib.models.MyUser;
import blog.lib.models.Post;
import blog.lib.models.Role;
import blog.lib.services.NotificationService;
import blog.lib.services.PostService;
import blog.lib.services.UserService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.Collection;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.not;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

//@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc
public class PostControllerTest {
    @InjectMocks
    private PostsController postsController;

    @Mock
    private NotificationService notifyService;

    @Mock
    private PostService postService;

    @Mock
    private UserService userService;

    @Mock
    Authentication auth;

    private MockMvc mockMvc;

    private MyUser currentUser;
    private MyUser testUser;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(postsController).build();

        // Sample users
        ArrayList<MyUser> users = new ArrayList<>();
        users.add(new MyUser(1L, "johndoe", "John Doe"));
        users.add(new MyUser(2L, "alicebog", "Alice Bog"));

        // Mock userService
        currentUser = users.get(0);
        testUser = users.get(1);
        when(userService.findByUsername(currentUser.getUsername())).thenReturn(currentUser);

        // Mock authentication
        Authentication authentication = mock(Authentication.class);
        SecurityContext securityContext = mock(SecurityContext.class);
        SecurityContextHolder.setContext(securityContext);
        when(securityContext.getAuthentication()).thenReturn(authentication);
        when(authentication.getPrincipal()).thenReturn(currentUser);
        when(authentication.getName()).thenReturn(currentUser.getUsername());
    }

    @After
    public void reset_mocks() {
        Mockito.reset(notifyService);
        Mockito.reset(postService);
        Mockito.reset(userService);
        Mockito.reset(auth);
    }

    @Test
    public void NonExistingPostIdRedirectsToHomeTest() throws Exception {
        Mockito.when(postService.findById(32L)).thenReturn(null);
        mockMvc.perform(post("/posts/view/32")).andExpect(redirectedUrl("/"));

    }

    @Test
    public void PostWithNoHTMLGetsParsed() throws Exception {
        Post post = new Post(33L, "test", "**test**", null);
        post.setHTMLbody("");
        Mockito.when(postService.findById(33L)).thenReturn(post);
        mockMvc.perform(post("/posts/view/33")).andExpect(status().isOk());
        assertThat(post.getHTMLbody()).isEqualTo("<p><strong>test</strong></p>\n");
    }

    @Test
    public void NonExistingPostCannotBeDeleted() throws Exception {
        Mockito.when(postService.findById(35L)).thenReturn(null);
        mockMvc.perform(get("/posts/delete/35")).andExpect(status().isFound()).andExpect(redirectedUrl("/posts"));
    }

    @Test
    public void ExistingPostCannotBeDeletedWithoutLogin() throws Exception {
        Authentication authentication = mock(Authentication.class);
        SecurityContext securityContext = mock(SecurityContext.class);
        SecurityContextHolder.setContext(securityContext);
        when(securityContext.getAuthentication()).thenReturn(authentication);
        when(authentication.getPrincipal()).thenReturn(null);
        when(authentication.getName()).thenReturn(null);

        Post post = new Post(34L, "test", "**test**", currentUser);
        Mockito.when(postService.findById(34L)).thenReturn(post);
        mockMvc.perform(get("/posts/delete/34")).andExpect(forwardedUrl("error/401")); //todo
    }

    @Test
    public void ExistingPostCannotBeDeletedByTheNonAuthorUser() throws Exception {

        when(userService.findById(2L)).thenReturn(testUser);
        Post post = new Post(36L, "test", "**test**", testUser);
        Mockito.when(postService.findById(36L)).thenReturn(post);
        mockMvc.perform(get("/posts/delete/36")).andExpect(redirectedUrl("/posts/")); //todo
        verify(notifyService,times(1)).addErrorMessage("Not allowed to delete post "+ 36);;
    }

    @Test
    public void ExistingPostCanBeDeletedByTheNonAuthorAdmin() throws Exception {
        Authentication authentication = mock(Authentication.class);
        SecurityContext securityContext = mock(SecurityContext.class);
        SecurityContextHolder.setContext(securityContext);
        when(securityContext.getAuthentication()).thenReturn(authentication);
        when(authentication.getPrincipal()).thenReturn(currentUser);
        when(authentication.getName()).thenReturn(currentUser.getUsername());



        this.currentUser.setRole(new Role(1,"ADMIN"));

        Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        authorities.add(new SimpleGrantedAuthority(currentUser.getRole().getRoleName()));

        doReturn(authorities).when(authentication).getAuthorities();

        when(userService.findById(2L)).thenReturn(testUser);
        Post post = new Post(37L, "test", "**test**", testUser);
        Mockito.when(postService.findById(37L)).thenReturn(post);
        mockMvc.perform(get("/posts/delete/37")).andExpect(redirectedUrl("/posts")); //todo
        verify(postService,times(1)).deleteById(37L);
        verify(notifyService,times(1)).addInfoMessage("Post deleted");
    }

    @Test
    public void PostWithNoTitleSetIsRejected() throws Exception {
        mockMvc
                .perform(
                        post("/posts/create")
                                .param("title", "")
                                .param("body", "**test**")
                                .param("author", currentUser.getUsername())
                )
                .andExpect(model().attributeHasFieldErrors("post", "title"))
                .andExpect(view().name("/posts/createNew"))
                .andExpect(status().isOk());
    }

}