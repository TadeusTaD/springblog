package blog.UnitTests;

import blog.app.controllers.HomeController;
import blog.lib.models.MyUser;
import blog.lib.models.Post;
import blog.lib.services.PostService;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;


//@SpringBootTest( webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT )
@WebMvcTest
@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc
public class HomeControllerTest {

    //@Autowired
    //private TestRestTemplate restTemplate;

    @InjectMocks
    private HomeController homeController;

    @Mock
    private PostService postService;

    @Autowired
    private MockMvc mockMvc;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(homeController)
                .setCustomArgumentResolvers(new PageableHandlerMethodArgumentResolver())
                .build();

        MyUser testUser= new MyUser(1L, "johndoe", "John Doe");

        ArrayList<Post> posts = new ArrayList<>();
        posts.add(new Post(1L, "test", "**test**", testUser));
        posts.add(new Post(2L, "test2", "**test2**", testUser));
        posts.add(new Post(3L, "test3", "**test3**", testUser));
        posts.add(new Post(4L, "test4", "**test4**", testUser));
        posts.add(new Post(5L, "test5", "**test5**", testUser));

        when(postService.findLatest5()).thenReturn(posts);
    }

    @Test
    public void indexShouldContainRecentPostsTest() throws Exception {
        this.mockMvc.perform(get("/")).andDo(print()).andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.model().attribute("latestFivePosts",postService.findLatest5()))
                .andExpect(MockMvcResultMatchers.model().size(2));


    }
}