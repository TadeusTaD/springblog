package blog.IntegrationTests;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.ServletContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.assertj.core.api.Assertions.assertThat;

//@SpringBootTest(classes = SpringBlogApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT )
@WebAppConfiguration
//@ContextConfiguration(classes = { MvcConfig.class })
//@ContextConfiguration({"classpath*:applicationContext.xml"})
@ExtendWith(SpringExtension.class)
public class HomeControllerTest {


    @LocalServerPort
    private int port;

    //@Autowired
    //private TestRestTemplate restTemplate;

    @Autowired
    private WebApplicationContext webAppContext;


    private MockMvc mockMvc;

    @Before
    public void setup() {
        System.out.println("WAC = " + webAppContext);



    }

    @Test
    public void wacServletContextProvidesHomeControllerTest() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webAppContext).build();

        final ServletContext servletContext = webAppContext.getServletContext();

        Assert.assertNotNull(servletContext);
        Assert.assertTrue(servletContext instanceof MockServletContext);
        Assert.assertNotNull(webAppContext.getBean("HomeController"));
    }

    @Sql({ "schema.sql", "data.sql" })
    @Test
    public void indexShouldContainRecentPostsTest() throws Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webAppContext).build();
        this.mockMvc.perform(get("/")).andDo(print()).andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("YOU UP"));


    }
}
