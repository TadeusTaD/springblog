package blog.IntegrationTests;

import blog.SpringBlogApplication;
import blog.app.controllers.AuthController;
import blog.lib.models.MyUser;
import blog.lib.services.NotificationService;
import blog.lib.services.UserService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest(classes = SpringBlogApplication.class,webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ExtendWith(SpringExtension.class)
//@WebAppConfiguration
@ContextConfiguration(classes = { SpringBlogApplication.class })
//@AutoConfigureMockMvc
@EnableSpringDataWebSupport
public class AuthControllerTest {

    //@Mock
    Authentication auth;
    //@InjectMocks
    private AuthController authController;
    //@Mock
//    private UserService userService;
    //@Mock
  //  private NotificationService notifyService;
    @Autowired
    private WebApplicationContext webAppContext;
    @Autowired
    private MockMvc mockMvc;
    private MyUser currentUser;

    //ModelAndView mav=authCo/home/tadeustad/IdeaProjects/SpringBlog/src/test/java/blog/IntegrationTestsntroller.ha

    @Before
    public void setup() {
        //MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(authController).build();
    //    mockMvc = MockMvcBuilders.webAppContextSetup(webAppContext).build();



    }


    @Test
    public void OKWhenLoginDataIsValidTest() throws Exception {

        // Sample users
        ArrayList<MyUser> users = new ArrayList<>();
        users.add(new MyUser(1L, "johndoe", "John Doe"));
        users.get(0).setActive(false);
        // Mock userService
        currentUser = users.get(0);

        // Mock authentication
        //Authentication authentication = mock(Authentication.class);
        //SecurityContext securityContext = mock(SecurityContext.class);
        //SecurityContextHolder.setContext(securityContext);
        ////when(securityContext.getAuthentication()).thenReturn(authentication);

        ////when(authentication.getPrincipal()).thenReturn(currentUser);
        ////when(authentication.getName()).thenReturn(currentUser.getUsername());

        ////when((userService.findByUsername(any(String.class)))).thenReturn(currentUser);

        mockMvc.perform(
                post("/users/login")
                        .param("userName", "johndoe")
                        .param("passwordHash", "aaaaaaaaaaaaaaa")
        )
                .andExpect(model().hasNoErrors())
                .andExpect(status().isFound());
    }

    @Test
    public void ErrorWhenLoginDataIsInvalidTest() throws Exception {

        // Mock userService
        //when(userService.findByUsername("john1")).thenReturn(null);

        // Mock authentication
        //Authentication authentication = mock(Authentication.class);
        //SecurityContext securityContext = mock(SecurityContext.class);
        //SecurityContextHolder.setContext(securityContext);
        //when(securityContext.getAuthentication()).thenReturn(authentication);

        ////when(authentication.getPrincipal()).thenReturn(currentUser);
        ////when(authentication.getName()).thenReturn(currentUser.getUsername());

        mockMvc.perform(
                post("/users/login")
                        .param("username", "john1")
                        .param("passwordHash", "aaaaaaaaa")
        )
                .andExpect(view().name("users/login"))
                .andExpect(status().isOk());

    }

    @Test
    public void OKWhenRegistrationDataIsValidTest() throws Exception {

        // Sample users
        ArrayList<MyUser> users = new ArrayList<>();
        users.add(new MyUser(1L, "johndoe", "John Doe"));
        users.get(0).setActive(false);
        // Mock userService
        currentUser = users.get(0);
        ////when(userService.findByUsername(currentUser.getUsername())).thenReturn(currentUser);

        mockMvc.perform(
                post("/users/register")
                        .param("username", "test")
                        .param("passwordHash", "aaaaaaaaaaaaaaa")
                        .param("fullName", "Test testowy")
        )
                .andExpect(model().attributeExists("successMessage"))
                .andExpect(view().name("users/register"))
                .andExpect(status().isOk());

    }

    @Test
    public void ErrorWhenRegistrationDataIsInvalidTest() throws Exception {
        // Sample users
        ArrayList<MyUser> users = new ArrayList<>();
        users.add(new MyUser(1L, "johndoe", "John Doe"));
        users.get(0).setActive(false);
        // Mock userService
        currentUser = users.get(0);
        ////when(userService.findByUsername(currentUser.getUsername())).thenReturn(currentUser);

        mockMvc.perform(
                post("/users/register")
                        .param("username", "johndoe")
                        .param("passwordHash", "bbbbbbbbbbbbbbbb")
                        .param("fullName", "john doe")
        )
                .andExpect(model().attributeHasFieldErrors("myUser", "username"))
                .andExpect(status().isOk());

    }
}
