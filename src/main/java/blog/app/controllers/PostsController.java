package blog.app.controllers;

import blog.MarkdownParse;
import blog.lib.models.Post;
import blog.lib.models.MyUser;
import blog.lib.services.NotificationService;
import blog.lib.services.PostService;
import blog.lib.services.UserService;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.Authentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;

@Controller
public class PostsController {
    private MarkdownParse markdownParse = new MarkdownParse();
    @Autowired
    private PostService postService;

    @Autowired
    private NotificationService notifyService;

    @Autowired
    private UserService userService;

    @RequestMapping("/posts/view/{id}")
    public String view(@PathVariable("id") Long id, Model model) {
        Post post = postService.findById(id);
        if(post==null){
            notifyService.addErrorMessage("Cannot find post "+ id);
            return "redirect:/";
        }
        if(post.getHTMLbody().isEmpty()) post.setHTMLbody(markdownParse.getResult(post.getBody()));
        model.addAttribute("post",post);
        return "/posts/view";
    }

    @RequestMapping("/posts")
    public String list(Model model, @PageableDefault(sort = {"postDate"}, value = 5) Pageable pageable) {
        Page<Post> posts = postService.FindAll(pageable);
        for (Post post:posts){
            post.setHTMLbody(markdownParse.getResult(post.getBody()));
        }

        model.addAttribute("posts",posts);
        return "posts";
    }


    @RequestMapping(value = "/posts/create",method = RequestMethod.POST)
    public ModelAndView create(@ModelAttribute Post post, BindingResult bindingResult) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/posts/createNew");

        if (post.getTitle().isEmpty()) {
            bindingResult.rejectValue("title", "error.post", "Post title cannot be empty!");
        }

        if (post.getBody().isEmpty()) {
            bindingResult.rejectValue("body", "error.post", "Post content cannot be empty!");
        }

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        MyUser user = this.userService.findByUsername(auth.getName());

        if (user==null) {
            bindingResult.rejectValue("author", "error.post", "Author is null!");
        }

        if(!bindingResult.hasErrors()) {
            post.setEditDate(null);
            post.setAuthor(user);
            notifyService.addInfoMessage("Post added");

            this.postService.create(post);
            modelAndView.addObject("post",new Post());
        }
            return modelAndView;
    }

    @RequestMapping(value = "/posts/create")
    public ModelAndView create() {
        ModelAndView modelAndView = new ModelAndView();
        Post post = new Post();

        modelAndView.addObject(post);
        modelAndView.setViewName("/posts/createNew");
        return modelAndView;
    }

    @RequestMapping(value = "/posts/edit/{id}")
    public String edit(@PathVariable("id") Long id, Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        MyUser siteUser = this.userService.findByUsername(auth.getName());

        if (siteUser==null) {
            return "error/401";
        }

        Post post = this.postService.findById(id);
        if(post==null){
            notifyService.addErrorMessage("Cannot find post "+ id);
            return "redirect:/posts/";
        }

        MyUser user = this.userService.findById(post.getAuthor().getId());
        if(
                (! (user.getUsername().equals(auth.getName())) )
            &&
                (! (auth.getAuthorities().stream()
                        .anyMatch
                            (role -> role.getAuthority().equals("ADMIN"))
                    )
                )
        )
        {
            notifyService.addErrorMessage("Not allowed to edit post "+ id);
            return "redirect:/posts/";
        }

        model.addAttribute("post",post);
        return "posts/edit";
    }

    @RequestMapping(value = "/posts/edit",method = RequestMethod.POST)
    public ModelAndView edit(@ModelAttribute Post post, BindingResult bindingResult) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("posts/edit");
        if (post.getTitle().isEmpty()) {
            bindingResult.rejectValue("title", "error.post", "Post title cannot be empty!");
        }

        if (post.getBody().isEmpty()) {
            bindingResult.rejectValue("body", "error.post", "Post content cannot be empty!");
        }

        if (post.getId() == null) {
            bindingResult.rejectValue("id", "error.post", "Post id cannot be empty!");
        }

        if (post.getAuthor() == null) {
            bindingResult.rejectValue("author", "error.post", "Post author cannot be empty!");
        }
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        MyUser user = this.userService.findById(post.getAuthor().getId());

        if (user == null) {
            bindingResult.rejectValue("author", "error.post", "Author is null!");
        }

        if(!bindingResult.hasErrors()){
                post.setAuthor(user);
                postService.edit(post);
                notifyService.addInfoMessage("Post edited");
                modelAndView.addObject("post", new Post());
                modelAndView.setViewName("redirect:/posts");
            }
            return modelAndView;
     }

    @RequestMapping(value = "/posts/delete/{id}", method = RequestMethod.GET)
    public String delete(@PathVariable("id") Long id) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        MyUser siteUser = this.userService.findByUsername(auth.getName());

        if (siteUser==null) {
            return "error/401";
        }

        Post post = postService.findById(id);
        if(post==null){
            notifyService.addErrorMessage("Cannot find post "+ id);
            return "redirect:/posts";
        }

        MyUser user = this.userService.findById(post.getAuthor().getId());

        if (
                (!(user.getUsername().equals(auth.getName())) )
                        &&
                        (!auth.getAuthorities().stream()
                                .anyMatch(role -> role.getAuthority().equals("ADMIN"))
                        )
        ) {
            notifyService.addErrorMessage("Not allowed to delete post "+ id);
            return "redirect:/posts/";
        }

        postService.deleteById(id);
        notifyService.addInfoMessage("Post deleted");
        return "redirect:/posts";
    }

}
