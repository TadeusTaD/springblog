package blog.app.controllers;

import blog.app.forms.LoginForm;
import blog.lib.models.MyUser;
import blog.lib.services.NotificationService;
import blog.lib.services.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


import javax.validation.Valid;

@Controller
public class AuthController {

    @Autowired
    private UserService userService;

    @Autowired
    private NotificationService notifyService;


    @RequestMapping(value = "/users/login", method = RequestMethod.POST)
    public String loginPage() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if ( (userService.findByUsername(auth.getName()) == null ) || (userService.findByUsername(auth.getName()).isActive()) ) {
            return "users/login"; }
        else {
            notifyService.addInfoMessage("Login successful");
            return "redirect:/"; }
    }



    @RequestMapping(value = "/users/login")
    public String loginPage(LoginForm loginForm) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if ((auth instanceof AnonymousAuthenticationToken)) {
            return "users/login"; }
        else {
            return "redirect:/"; }
    }


    @RequestMapping("/users/register")
    public String registration(Model model){
        MyUser user = new MyUser();
        model.addAttribute("user",user);
        return "/users/register";
    }
    @RequestMapping(value = "users/register", method = RequestMethod.POST)
    public ModelAndView registration(@Valid @ModelAttribute MyUser user, BindingResult bindingResult){
        ModelAndView modelAndView = new ModelAndView();
        MyUser userExists = this.userService.findByUsername(user.getUsername());
        modelAndView.setViewName("users/register");
        if( userExists != null ){
            bindingResult.rejectValue("username", "error.user", "User already exists!");
        }
        if( !bindingResult.hasErrors() ){
            this.userService.create(user);
            modelAndView.addObject("successMessage", "User has been created");
            modelAndView.addObject("user", new MyUser());
            notifyService.addInfoMessage("Registration successful");
        }
        return modelAndView;

    }
}


