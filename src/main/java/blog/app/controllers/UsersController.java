package blog.app.controllers;

import blog.lib.models.MyUser;
import blog.lib.services.NotificationService;
import blog.lib.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class UsersController {

    @Autowired
    private UserService userService;

    //@Autowired
    //private NotificationService notifyService;




    @RequestMapping("/users")
    public String view(Model model, @PageableDefault(sort = {"username"}, value = 10) Pageable pageable) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        MyUser user = this.userService.findByUsername(auth.getName());

        if (user==null) {
            return "error/401";
        }

        Page<MyUser> users = this.userService.FindAll(pageable);

        model.addAttribute("users", users);
        return "users/index";
    }

}
