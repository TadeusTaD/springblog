package blog;
//import org.commonmark.Extension;
//import org.commonmark.ext.gfm.strikethrough.StrikethroughExtension;

import org.commonmark.node.*;
import org.commonmark.parser.Parser;
import org.commonmark.renderer.html.HtmlRenderer;

import java.util.Arrays;
import java.util.List;

public class MarkdownParse {

   private String result;

    public String getResult(String input) {
        getHTML(input);
        return this.result;
    }


    public void getHTML(String input) {
        /*List<Extension> extensions = Arrays.asList(
         StrikethroughExtension.create()
        );*/

        Parser parser = Parser.builder().build();
        //Node document = parser.parse(input);
        HtmlRenderer renderer = HtmlRenderer.builder().build();
        this.result = renderer.render(parser.parse(input));
    }
}
