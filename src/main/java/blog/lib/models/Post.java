package blog.lib.models;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "posts")
public class Post {
    @Override
    public String toString() {
        return "Post{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", body='" + body + '\'' +
                ", author=" + author +
                ", postDate=" + postDate +
                '}';
    }

    public Post(Long id, String title, String body, MyUser author) {
        this.id = id;
        this.title = title;
        this.body = body;
        this.author = author;
        this.HTMLbody = "";
    }

    public Post(Post post) {
        this.id = post.id;
        this.title = post.title;
        this.body = post.body;
        this.author = post.author;
        this.HTMLbody = "";
    }
    public Post() {}

    public Long getId() {return id;}
    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }
    public void setBody(String postBody) {
        this.body = postBody;
    }

    public MyUser getAuthor() {
        return author;
    }
    public void setAuthor(MyUser author) {
        this.author = author;
    }

    public Date getPostDate() {
        return postDate;
    }
    public void setPostDate(Date date) {
        this.postDate = date;
    }

    public String getHTMLbody() {
        return HTMLbody; }

    public void setHTMLbody(String HTMLbody) {
        this.HTMLbody = HTMLbody; }


    public Date getEditDate() {
        return editDate;
    }

    public void setEditDate(Date editDate) {
        this.editDate = editDate;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 300)
    private String title;

    @Lob
    @Column(nullable = false, length = 8000)
    private String body;

    @Lob
    @Column(nullable = true, length = 8000)
    private String HTMLbody;


    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private MyUser author;

    @Column(nullable = false, updatable = false)
    private Date postDate = new Date();


    @Column(nullable = true)
    private Date editDate = new Date();
}
