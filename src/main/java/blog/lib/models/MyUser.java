package blog.lib.models;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="users")
public class MyUser {

    public String getUsername() {return username;}
    public void setUsername(String username) {this.username = username;}

    public String getPasswordHash() {return passwordHash;}
    public void setPasswordHash(String passwordHash) {this.passwordHash = passwordHash;}

    public String getFullName() {return fullName;}
    public void setFullName(String fullName) {this.fullName = fullName;}

    public Set<Post> getPosts() {return posts;}
    public void setPosts(Set<Post> posts) {this.posts = posts;}

    public Long getId() {return id;}
    public void setId(Long id) {this.id = id;}

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public MyUser() { }

    public MyUser(Long id, String username, String fullName) {
        this.id = id;
        this.username = username;
        this.fullName = fullName;
        this.active = true;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private boolean active;

    @Column(nullable = false, length = 30, unique = true)
    private String username;


    @Column(length = 60)
    private String passwordHash;


    @Column(length = 100)
    private String fullName;

    @OneToOne(cascade=CascadeType.MERGE)
    private Role role;

    @OneToMany(mappedBy = "author")
    private Set<Post> posts = new HashSet<>();

    @Override
    public String toString() {
        return "MyUser{" +
                "id=" + id +
                ", active=" + active +
                ", username='" + username + '\'' +
                ", passwordHash='" + passwordHash + '\'' +
                ", fullName='" + fullName + '\'' +
                ", role=" + role +
                '}';
    }
}
