package blog.lib.services;

import blog.lib.models.MyUser;
import blog.lib.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    private UserRepository userRepository;

    private User EntityToUser(MyUser myUser) {
        String username = myUser.getUsername();
        String password = myUser.getPasswordHash();
        boolean enabled = myUser.isActive();
        boolean expired = myUser.isActive();
        boolean credNotExpired = myUser.isActive();
        boolean locked = myUser.isActive();
        Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
            authorities.add(new SimpleGrantedAuthority(myUser.getRole().getRoleName()));
        User user=new User(username,password,enabled,expired,credNotExpired,locked,authorities);
        return user;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        MyUser user = userRepository.findByUsername(username);
        if (user == null) throw new UsernameNotFoundException("User not found");
        return EntityToUser(user);
    }
}
