package blog.lib.services;

import blog.lib.models.MyUser;
import blog.lib.models.Role;
import blog.lib.repositories.RoleRepository;
import blog.lib.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Primary
public class UserServiceJpa implements UserService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;


    @Override
    public Page<MyUser> FindAll(Pageable pageable) {
        return this.userRepository.findAll(pageable);
    }

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public MyUser create(MyUser user) {
        Role role = roleRepository.findByRoleName("USER");
        user.setActive(true);
        user.setRole(role);
        user.setPasswordHash(bCryptPasswordEncoder.encode(user.getPasswordHash()));
        return this.userRepository.save(user);
    }

    @Override
    public List<MyUser> FindAll() {
        return this.userRepository.findAll();
    }

    @Override
    public MyUser findById(Long id) {
        return this.userRepository.findById(id).orElse(null);
    }

    @Override
    public MyUser findByUsername(String username) {
        return this.userRepository.findByUsername(username);
    }

    @Override
    public MyUser edit(MyUser user) {
        return this.userRepository.save(user);
    }

    @Override
    public void deleteById(Long id) {
        this.userRepository.deleteById(id);
    }

    @Override
    public boolean authenticate(String username, String password) {
        return true;
    }

    @Override
    public boolean register(String username, String password) {
        return true;
    }
}
