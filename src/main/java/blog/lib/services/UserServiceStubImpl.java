package blog.lib.services;

import blog.lib.models.MyUser;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class UserServiceStubImpl implements UserService {
    @Override
    public boolean register(String username, String password) {
        if (username.equals(password)){
            return Objects.equals(username,password);
        }
        return Objects.equals(username,password);
    }

    @Override
    public List<MyUser> FindAll() {
        return null;
    }

    @Override
    public Page<MyUser> FindAll(Pageable pageable) {
        return null;
    }

    @Override
    public MyUser findById(Long id) {
        return null;
    }

    @Override
    public MyUser findByUsername(String username) {
        return null;
    }

    @Override
    public MyUser create(MyUser user) {
        return null;
    }

    @Override
    public MyUser edit(MyUser user) {
        return null;
    }

    @Override
    public void deleteById(Long id) {

    }

    @Override
    public boolean authenticate(String username, String password) {
        if (username.equals(password)){
            return Objects.equals(username,password);
        }
        return Objects.equals(username,password);
    }
}
