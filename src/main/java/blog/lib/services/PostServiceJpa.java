package blog.lib.services;

import blog.lib.models.Post;
import blog.MarkdownParse;
import blog.lib.repositories.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Primary
public class PostServiceJpa implements PostService {
    @Autowired
    private PostRepository postRepository;
    private MarkdownParse markdownParse = new MarkdownParse();

    @Override
    public Post create(Post post) {
        if (post.getBody() != null) post.setHTMLbody(markdownParse.getResult(post.getBody()));
        return this.postRepository.saveAndFlush(post);
    }

    @Override
    public Page<Post> FindAll(Pageable pageable) { return this.postRepository.findAll(pageable); }

    @Override
    public List<Post> FindAll() {
        return this.postRepository.findAll();
    }

    @Override
    public List<Post> findLatest5() {
        return this.postRepository.findLatest5Posts(PageRequest.of(0,5));
    }

    @Override
    public Post findById(Long id) {
        return this.postRepository.findById(id).orElse(null);
    }

    @Override
    public Post edit(Post post) {
        if (post.getBody() != null) post.setHTMLbody(markdownParse.getResult(post.getBody()));
        return this.postRepository.saveAndFlush(post);
    }

    @Override
    public void deleteById(Long id) {
        this.postRepository.deleteById(id);
    }
}
