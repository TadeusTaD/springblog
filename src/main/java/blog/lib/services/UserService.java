package blog.lib.services;

import blog.lib.models.MyUser;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


import java.util.List;

public interface UserService {
    List<MyUser> FindAll();
    Page<MyUser> FindAll(Pageable pageable);
    MyUser findById(Long id);
    MyUser findByUsername(String username);
    MyUser create(MyUser user);
    MyUser edit(MyUser user);
    void deleteById(Long id);
    boolean authenticate(String username, String password);
    boolean register(String username, String password);
}
