package blog.lib.services;

import blog.lib.models.Post;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface PostService {
    Post create(Post post);

    List<Post> FindAll();
    Page<Post> FindAll(Pageable pageable);
    List<Post> findLatest5();

    Post findById(Long id);

    Post edit(Post post);

    void deleteById(Long id);

}
