package blog.lib.services;

import blog.lib.models.Post;
import blog.lib.models.MyUser;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class PostServiceStub implements PostService {
    private static Long lastId=0L;
    private List<Post> posts = new ArrayList<Post>() {{
       add(new Post(1L,"First post", "<p>Hello, world!</p>", null));
       add(new Post(2L,"Test post", "<p>Lorem ipsum dolor sit amet</p>", new MyUser(10L, "zbigniew1", "Zbigniew Nowak")));
    }};

    @Override
    public Post create(Post post) {
        if (!this.posts.isEmpty()) lastId=this.posts.stream().mapToLong(p->p.getId()).max().getAsLong();
        post.setId(lastId+1);
        this.posts.add(post);
        return post;
    }

    @Override
    public Post edit(Post post) {
        for(Post p: posts)
        {
            if(Objects.equals(p.getId(),post.getId())) {
                this.posts.set(this.posts.indexOf(p),post);
            return post;
            }
        }
        throw new RuntimeException("Post not found: " + post.getId() );
    }

    @Override
    public void deleteById(Long id) {
        if(this.posts.size()==1) lastId=this.posts.get(0).getId();
    for(int i=0; i<this.posts.size();i++){
        if(Objects.equals(this.posts.get(i).getId(),id)){
            this.posts.remove(i);
            }
        }

    }
    @Override
    public List<Post> FindAll() {
        return this.posts;
    }

    @Override
    public Page<Post> FindAll(Pageable pageable) {
        return null;
    }

    @Override
    public List<Post> findLatest5() {
        return this.posts.stream()
                .sorted(
                        (p1,p2) -> p2.getPostDate().compareTo(p1.getPostDate())
                        ).limit(5)
                        .collect(Collectors.toList());
    }

    @Override
    public Post findById(Long id) {
        return this.posts.stream().filter(p -> Objects.equals(p.getId(), id))
                .findFirst()
                .orElse(null);
    }


}
