package blog.lib.repositories;

import blog.lib.models.Post;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.domain.Page;

import java.util.List;

@Repository
public interface PostRepository extends JpaRepository<Post, Long> {
    @Query("SELECT post FROM Post post LEFT JOIN FETCH post.author ORDER BY post.postDate DESC")
    List<Post> findLatest5Posts(Pageable pageable);

    @Query("SELECT post FROM Post post ORDER BY post.postDate DESC")
    Page<Post> findAll(Pageable pageable);
}
