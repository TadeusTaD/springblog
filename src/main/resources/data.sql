INSERT INTO `roles` (`id`, `role_name`) VALUES (1,'ADMIN'),(2,'USER');
INSERT INTO `users` (`id`, `full_name`, `password_hash`, `username`, `active`, `role_id`) VALUES (16,'Admin Admin','$2a$10$2Jy2YDM0BPDnsVDup29NO.UD96k.IKrWRt8o1k.fxbbx7yURZwulK','admin',CONV('1', 2, 10) + 0,1);
INSERT INTO `posts` (`id`, `body`, `edit_date`, `post_date`, `title`, `author_id`, `htmlbody`) VALUES (1,'Hello world!  \r\n','2020-07-30 11:51:49.765000','2020-07-30 11:51:49.765000','Hello',16,' ');
